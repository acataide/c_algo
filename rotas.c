#include <stdlib.h>
#include <stdio.h>

#define y_max 10
#define x_max 80

typedef struct Pos{
    int y;
    int x;
} Pos;

typedef struct No{
    Pos pos;
    struct No *pai;
} No;

void printL(char labirinto[y_max][x_max]){
    int x, y; 
    for (y=0;y<y_max;++y){
        for (x=0;x<x_max;++x){
            printf("%c", labirinto[y][x]);
        }
        printf("\n");
    }
}

void gera_labirinto(char labirinto[y_max][x_max], char espaco, char parede, int prop){
    int x, y; 
    for (y=0;y<y_max;++y){
        for (x=0;x<x_max;++x){
            if (rand() % prop == 0){
                labirinto[y][x] = parede;
            }else{
                labirinto[y][x] = espaco;
            }
        }
    }
}

No *novo_no(Pos pos, No *pai){
    No *no = malloc(sizeof(No));
    no->pos = pos;
    no->pai = pai;
}

No *resolver(No *no, char labirinto[y_max][x_max], Pos final ){
    if (no != NULL){
        if (no->pos.y == final.y && no->pos.x == final.x) return no;
    } else {
        return NULL;
    }

    Pos prox[4] = {{no->pos.y-1, no->pos.x},
                   {no->pos.y, no->pos.x+1},
                   {no->pos.y+1, no->pos.x},
                   {no->pos.y, no->pos.x-1}};

    int i;
    No *res;
    for (i=0;i<4;++i){
        if (prox[i].x >= 0 && prox[i].y >= 0 &&
            prox[i].x < x_max && prox[i].y < y_max &&
            labirinto[prox[i].y][prox[i].x] == ' '){
                //printf("(%d,%d)\n", prox[i].y, prox[i].x);
                labirinto[prox[i].y][prox[i].x] = '_';
                res = resolver(novo_no(prox[i], no), labirinto, final);
                if (res != NULL) return res;
        }
    }
    return NULL;
}

int main(){
    Pos inic = {0,0};
    Pos fina = {y_max-1,x_max-1};

    char labirinto[y_max][x_max];
    gera_labirinto(labirinto, ' ', 'H', 5);

    No *raiz = novo_no(inic, NULL);
    
    No *resultado = resolver(raiz, labirinto, fina);
    
    while (resultado != NULL){
        labirinto[resultado->pos.y][resultado->pos.x] = '+';
        resultado = resultado->pai;
    }
    
    labirinto[inic.y][inic.x] = 'I';
    labirinto[fina.y][fina.x] = 'F';

    printL(labirinto);

    return 0;
}