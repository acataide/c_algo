#include <stdlib.h>
#include <stdio.h>

int in(char* str, char c){
    int i = 0;
    while (str[i] != 0){
        if (str[i] == c) return i;
        i++;
    }
    return -1;
}
int lim(int val, int max){
    if (val >= 0) {
        return val % max;
    } else {
        return max + (val % max);
    }
}

int main() {
    int senha, i;
    char msg[2000];
    char val[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";

    puts("Digite um numero senha: ");
    scanf("%d", &senha);
    while((getchar())!='\n');

    puts("Digite uma mensagem:");
    fgets(msg, 2000, stdin);

    for (i=0; i<2000; ++i) {
        if (msg[i] == 0) break;
        int c = in(val, msg[i]);
        if ( c != -1) {
            printf("%c", val[lim(c+senha,27)]);
        }
    }
    return 0;
}