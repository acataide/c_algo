#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int contador = 0;

void gera_lista(int *lista, int tam){
    int i;
    for (i=0; i<tam; i++){
        lista[i] = rand()% (tam+1);
    }
}

void printl(int *lista, int tam){
    int i;
    printf("{ ");
    tam--;
    for (i=0; i<tam; i++){
        printf("%d, ", lista[i]);
    }
    printf("%d }", lista[tam]);
}

void bubble_sort(int *lista, int tam){
    int i, j, temp;

    for (i=0; i < tam - 1; i++){
        for (j=0; j < tam - i - 1; j++){
            contador++;
            if (lista[j] > lista[j+1]){
                temp = lista[j];
                lista[j] = lista[j+1];
                lista[j+1] = temp;
            } 
        }
    }
}

void bubble_sort2(int *lista, int tam){
    int i, j, temp;
    bool ordenado = true;

    i = 0;
    while (true){
        ordenado = true;
        for (j=0; j < tam - i - 1; j++){
            contador++;
            if (lista[j] > lista[j+1]){
                temp = lista[j];
                lista[j] = lista[j+1];
                lista[j+1] = temp;
                ordenado = false;
            }
        }
        if (ordenado == true) return;
        i++; 
    }
}

int main() {
    int tam;
    int *lista;
    int i;

    printf("Digite o tamanho da lista: ");
    scanf("%d", &tam);

    lista = malloc(sizeof(int)*tam);
    
    gera_lista(lista, tam);
    printl(lista, tam);
    printf("\n\n");
    bubble_sort2(lista, tam);
    printl(lista, tam);
    printf("\nContador=%d\n", contador);
    return 0;
}