#include <stdio.h>
#include <stdlib.h>

typedef struct Cel {
	int cont;
	struct Cel *p;
} Cel;

Cel *newLista() {
	Cel *cel = malloc(sizeof(Cel));
	cel->p = NULL;
	return cel;
}

void insere(int cont, Cel *cel){
	if (cel == NULL) return;
	Cel *n_cel = malloc(sizeof(Cel));
	n_cel->cont = cont;
	n_cel->p = cel->p;
	cel->p = n_cel;
}

void remover(Cel *cel){
	if (cel == NULL || cel->p == NULL) return;
	Cel *lixo = cel->p;
	cel->p = lixo->p;
	free(lixo);
}


void printl(Cel *cel) {
	Cel *elem = cel->p;
	while (elem != NULL) {
		printf("%d, ", elem->cont);
		elem = elem->p;
	}
}

Cel *busca(Cel *cel, int val) {
	Cel *elem = cel->p;
	while (elem != NULL && elem->cont != val) {
		elem = elem->p;
	}
	return elem;
}

int main(int argc, char *argv[]) {
	int i;
	
	Cel *lista = newLista();
	for (i=20; i>0; --i){
		insere(i, lista);
	}	
	printl(lista);
	

	return 0;
}
