#include <stdlib.h>
#include <stdio.h>

#define quant 3

typedef struct {
    char nome[200];
    int idade;
    int rgm;
} Aluno;

int main () {
    int i;
    Aluno alunos[quant];
    printf("Tamanho aluno: %d \n", sizeof(Aluno));

    for (i=0; i<quant; ++i){
        printf("Aluno[%d]: nome, idade, rgm\n", i);
        scanf("%s %d %d", &alunos[i].nome, &alunos[i].idade, &alunos[i].rgm);
    }

    alunos[0].idade = 99;
    for (i=0; i<quant; ++i){
        printf("\n{%s, %d, %d}", alunos[i].nome,
                alunos[i].idade, alunos[i].rgm);
    }
    return 0;
}